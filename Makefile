#!/bin/bash
all: clientC

clientC: clientC.c
	gcc clientC.c -o clientC
	
test: clientC
	python serverP.py 4400 127.0.0.1:27017 db & pid1=$$!; sleep 2; ./clientC 20 127.0.0.1 4400 & pid2=$$!; sleep 60; kill $$pid1; wait $$pid1 && wait $$pid2 && echo OK && exit 0; echo FAIL;