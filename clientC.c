#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

void generate_message(char* msg, int len)
{
    int i;
    for(i = 0; i < len; i++)
    {
        msg[i] = rand() % ('Z' - 'A' + 1) + 'A';
    }
}

void error(const char *msg)
{
    fprintf(stderr, "%s", msg);
    exit(1);
}

int main(int argc, char *argv[])
{
    int N, port;
    int i, m;
    int sockfd;
    char message[21] = {0};
    struct sockaddr_in serv_addr;
    struct hostent *server;

    if (argc != 4)
    {
        error("Error in arguments\n");
    }
    sscanf(argv[1], "%d", &N);
    sscanf(argv[3], "%d", &port);

    srand(time(NULL));

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        error("ERROR opening socket");
    }
    server = gethostbyname(argv[2]);
    if (server == NULL)
    {
        error("ERROR, no such host\n");
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(sockfd,(struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        error("ERROR connecting");
    }

    for(i = 0; i < N; i++)
    {
        char response[5] = {0};
        generate_message(message, 20);

        printf("%s - ", message);


        m = write(sockfd, message, strlen(message));

        if (m < 0)
            error("ERROR writing to socket");
        m = read(sockfd, response, 4);
        if (m < 0)
            error("ERROR reading from socket");
        printf("%s\n",response);
        if (strcmp(response, "FAIL") == 0)
        {
            error("ERROR error at server");
        }
        sleep(1);
    }

    close(sockfd);
    return 0;
}
