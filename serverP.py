import socket
import signal
import logging
import sys
import time
from pymongo import MongoClient


class ServerP(object):
    def __init__(self, port, mongo_addr, mongo_name):
        host = ''
        self.soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.soc.bind((host, port))
        self.soc.listen(1)

        self.mongo_client = MongoClient(mongo_addr)
        self.db = self.mongo_client[mongo_name]

    def rcv(self):
        conn, addr = self.soc.accept()
        while True:
            msg = conn.recv(20)
            if not msg:
                break
            ip, port = socket.getnameinfo(addr, 0)
            logging.info(ip + ', ' + port + ', ' + msg)
            suc = True
            try:
                self.db.messages.insert_one({"ip": ip, "port": int(port), "timestamp": time.time(), "message": msg})
            except Exception as e:
                logging.error(e.args)
                suc = False

            if suc:
                conn.sendall("OK")
            else:
                conn.sendall("FAIL")

        conn.close()

    def __del__(self):
        self.soc.close()


def handler(signum, frame):
    print "Received terminate signal"
    exit(0)


def main():
    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)
    if len(sys.argv) != 4:
        logging.error("Error in arguments")
        exit(1)
    port = int(sys.argv[1])
    mongo_addr = sys.argv[2]
    mongo_name = sys.argv[3]
    try:
        server = ServerP(port, mongo_addr, mongo_name)
        while True:
            try:
                server.rcv()
            except Exception as e:
                logging.error(e.args)

    except Exception as e:
        logging.error(e.args)
        exit(1)


if __name__ == '__main__':
    main()
